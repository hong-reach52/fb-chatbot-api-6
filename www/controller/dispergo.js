const express = require('express');
const subRouterDispergo = require('express').Router();
const model = require("../model/datahandler");
const url = require('url');
const requestPromise = require('request-promise');
const chatfuelBroadcast = require('chatfuel-broadcast').default;
const path = require('path');
const randomstring = require("randomstring");
const uuid58 = require("uuid58");
const amqp = require('amqplib/callback_api');
const amqs = require('amqplib/callback_api');
require('../config/config');

var restUrl = "";

subRouterDispergo.use('/paynamics-error', express.static(path.join(__dirname, '../webview/error.html'),));
subRouterDispergo.use('/assets', express.static(path.join(__dirname, '../webview/assets'),));
subRouterDispergo.use('/css', express.static(path.join(__dirname, '../webview/css'),));
subRouterDispergo.use('/scripts', express.static(path.join(__dirname, '../webview/scripts'),));
subRouterDispergo.use('/images', express.static(path.join(__dirname, '../webview/images'),));
subRouterDispergo.use('/fonts', express.static(path.join(__dirname, '../webview/fonts'),));
subRouterDispergo.use('/less', express.static(path.join(__dirname, '../webview/less'),));
subRouterDispergo.use('/scss', express.static(path.join(__dirname, '../webview/scss'),));

subRouterDispergo .post('/dispergo_user_check', function(req, res) {
    console.log("routerDispergo post");
    console.log(req.body);
    model.fetch_user(
        function (result) {
           console.log("result:");
           console.log(result);
           if(result.hasOwnProperty("messenger_id") && result.hasOwnProperty("curis_id")) {
                if(req.body.validate == "1") {
           	    res.send({ "messages": [{"text":"User already exist. Please try again"}], "redirect_to_blocks": ["New User"] });
                } else {
           	    res.send({ "redirect_to_blocks": ["Existing User"] });
                } 
	   }
           else
           	res.send({ "redirect_to_blocks": ["New User"] });
    	}, req.body.messenger_id,
           req.body.curis_id);
});

subRouterDispergo.post('/dispergo_register', function(req, res) {
    console.log("routerDispergo post"); 
    /*var jsonResponse = {};
    model.fetch_user(
        function (result) {
            console.log("result:");
            console.log(result);
            if(result.hasOwnProperty("messenger_id") || result.hasOwnProperty("curis_id")) {
                if(req.body.validate == "1") {
                res.send({ "messages": [{"text":"User already exist. Please try again"}], "redirect_to_blocks": ["New User"] });
                } else {
                res.send({ "redirect_to_blocks": ["Existing User"] });
                } 
        }
            else {
                console.log("Saving data to dispergo_id");
                model.save_data("dispergo_id", req.body);
            }
        }, req.body.messenger_id,
       req.body.curis_id);*/
        const opt = { credentials: require('amqplib')
                     .credentials.plain(process.env.RABBIT_USERNAME, process.env.RABBIT_PASSWORD) };
        amqp.connect(`${process.env.RABBIT_PROTOCOL}://${process.env.RABBIT_HOST}:${process.env.RABBIT_PORT}`, opt, function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
                res.send({
                    "messages": [
                      {"text": "Error using tracking code" }
                    ]
                });
                throw error1;
            }

            var queue = 'facebook.orders.subscriptions';
            var msg = JSON.stringify(req.body);

            channel.assertQueue(queue, {
                durable: false
            });
            channel.sendToQueue(queue, Buffer.from(msg));
            res.send({
                "messages": [
                  {"text": "Your are now subscribed to receive updates for Tracking Code " + req.body.trackingCode },
                ],
                "set_attributes": {
                  "user_subscribed": "Subscribed"
                }
               });
            //connection.close();
        });
    });
});

subRouterDispergo.post('/notify_order', function(req, res) {
    const botId = process.env.BOT_ID;
    const chatfuelToken = process.env.CHATFUEL_TOKEN;

    const userId = req.body.messenger_id;
    const blockName = req.body.blockName;
    const attribObj = req.body.attributes;

    const broadcastApiUrl = process.env.CHATFUEL_BROADCAST_URL + `${botId}/users/${userId}/send`;
    
    var chatfuelCommonObj = {
        chatfuel_token: chatfuelToken,
        chatfuel_block_name: blockName,
        chatfuel_message_tag: (blockName == "Payment")? "PERSONAL_FINANCE_UPDATE" : (blockName == "Receipt")? "PAYMENT_UPDATE" : "ACCOUNT_UPDATE"
    };
    
    for (var p in attribObj) {
        if( attribObj.hasOwnProperty(p) ) {
            if(Array.isArray(attribObj[p]) || typeof attribObj[p] === 'object') {
                chatfuelCommonObj[p] = JSON.stringify(attribObj[p]);
            }
            else {
                chatfuelCommonObj[p] = attribObj[p];
            }
       } 
    } 
    var query = Object.assign(
        chatfuelCommonObj,
        req.body
    );

    var chatfuelApiUrl = url.format({
        pathname: broadcastApiUrl,
        query
    });

    var options = {
        uri: chatfuelApiUrl,
        headers: {
            'Content-Type': 'application/json'
        }
    };

    requestPromise.post(options)
        .then(() => {
            res.json({});
        });
});

/***********PAYMENT SECTION START*******************************/
const createPayButtons = (displayUrl) => {
    return {
        messages: [{
            attachment: {
                type: 'template',
                payload: {
                    template_type: 'generic',
                    image_aspect_ratio: 'square',
                    elements: [{
                        title: 'Do you want to re-order?',
                        subtitle: "Choose payment method below to show details of this order and to reorder again",
                        buttons: [{
                            type: 'web_url',
                            url: displayUrl[0],
                            title: 'Paynamics',
                            messenger_extensions: true,
                            webview_height_ratio: 'full', // Full view,
                            webview_share_button: 'hide'
                        },
                        {
                            type: 'web_url',
                            url: displayUrl[1],
                            title: 'Credit Card',
                            messenger_extensions: true,
                            webview_height_ratio: 'compact', // Small view,
                            webview_share_button: 'hide'
                        }]
                    },{
                        title: 'Do you want to re-order?',
                        subtitle: "Choose payment method below to show details of this order ID and to reorder again",
                        buttons: [{
                            type: 'web_url',
                            url: displayUrl[0],
                            title: 'GCash',
                            messenger_extensions: true,
                            webview_height_ratio: 'compact', // Small view,
                            webview_share_button: 'hide'
                        }]
                    }]
                }
            }
        }]
    };
};

const createGenericTemplateButtons = (templateType, elementContents) => {
    return {
        messages: [{
            attachment: {
                type: 'template',
                payload: {
                    template_type: templateType,
                    elements: elementContents
                }
            }
        }]
    };
};

var receiptFormat = (parameters) => {
    return {  
        messages: [  
           {  
              attachment: {  
                 type: "template",
                 payload: {  
                    template_type: "receipt",
                    recipient_name: "Resident Name",
                    order_number: parameters.payment_id,
                    currency: parameters.currency,
                    payment_method: parameters.payment_method,
                    order_url: "https://rockets.chatfuel.com/store?order_id=12345678901",
                    timestamp: Math.round(new Date().getTime()/1000),
                    address: parameters.address,
                    summary: parameters.summary,
                    adjustments :parameters.adjustments,
                    elements: parameters.elements
                 }
              }
           }
        ]
     }
};

subRouterDispergo.get('/random-payment-id', (request, response) => {
   payment_id = randomstring.generate({
  		   	length: 8,
  		   	charset: 'numeric'
		   });
    response.send({
            "set_attributes":  {
            "payment_id": payment_id
           }
    });

});


subRouterDispergo.get('/options', (request, response) => {
    const query = "userID=" + request.query.userID +
                 "&payment_id=" + request.query.payment_id +
                 "&currency=" + request.query.currency +
                 "&items=" + request.query.items +
                 "&currency=" + request.query.currency +
                 "&patientAddress=" + request.query.patientAddress +
                 "&patientAge=" + request.query.patientAge +
                 "&patientGender=" + request.query.patientGender +
                 "&patientName=" + request.query.patientName +
                 "&physicianId=" + request.query.physicianId +
                 "&physicianLicenseNumber=" + request.query.physicianLicenseNumber +
                 "&physicianName=" + request.query.physicianName +
                 "&prescriptionNumber=" + request.query.prescriptionNumber +
                 "&adjustments=" + request.query.adjustments +
                 "&residentId=" + request.query.residentId +
                 "&ecr_link=" + request.query.ecr_link;
    const displayUrl = [
        routerDispergo.restUrl + '/paynamics?' + query,
        routerDispergo.restUrl + '/ccard?' + query
    ];
    /*const displayUrl = [
                         routerDispergo.restUrl + '/ccard?userID=' + userId + '&payment_id=' + payment_id + '&type=order&order_id=' + order_id + '&currency=' + currency,
                         routerDispergo.restUrl + '/paymaya?userID=' + userId + '&payment_id=' + payment_id + '&type=order&order_id=' + order_id + '&currency=' + currency,
                         routerDispergo.restUrl + '/paypal?userID=' + userId + '&payment_id=' + payment_id + '&type=order&order_id=' + order_id + '&currency=' + currency
    ];*/
    response.json(createPayButtons (displayUrl));
});


subRouterDispergo.post('/pay-response', function(req, res) {
    console.log(req.body);
    const BOT_ID = process.env.BOT_ID;
    const CHATFUEL_TOKEN = process.env.CHATFUEL_TOKEN ;

    const userId = req.body.userID;
    const blockName = req.body.blockName;

    const broadcastApiUrl = process.env.CHATFUEL_BROADCAST_URL + `${BOT_ID}/users/${userId}/send`;

    const query = Object.assign({
            chatfuel_token: CHATFUEL_TOKEN,
            chatfuel_block_name: blockName
        },
        req.body
    );

    const chatfuelApiUrl = url.format({
        pathname: broadcastApiUrl,
        query
    });

    const options = {
        uri: chatfuelApiUrl,
        headers: {
            'Content-Type': 'application/json'
        }
    };

    requestPromise.post(options)
        .then(() => {
            res.json({});
        });
});

subRouterDispergo.get('/paynamics', function(req, res) {
    var userID = req.query.userID;
    var payment_id = "";//req.query.payment_id ;
    var trackingCode = req.query.trackingCode;
    var currency = req.query.currency;
    var items = JSON.parse(req.query.items);
    var patientAddress = req.query.patientAddress;
    var patientAge = req.query.patientAge;
    var patientGender = req.query.patientGender;
    var patientName = req.query.patientName;
    var physicianId = req.query.physicianId;
    var physicianLicenseNumber = req.query.physicianLicenseNumber;
    var physicianName = req.query.physicianName;
    var prescriptionNumber = req.query.prescriptionNumber;
    var residentId = req.query.residentId;
    var ecr_link = req.query.ecr_link;
    var is_paid = "false";
    var price_list = "";
    var item_list = "";
    var qty_list = "";
    var total_price = 0.0;
    var total_tax = 0.0;
    var total_shipping_cost = 0.0;
    var adjustments = req.query.adjustments;
    var elements = [];
    var medData = [];

    var options = {
        method: 'POST',
        uri: routerDispergo.restUrl + "/notify_order",
        body: {
            messenger_id : userID,
            blockName : "Blank",
            attributes : {
                "trackingCode" : trackingCode,
                "ecr_link": ecr_link,
                "items": items,
                "patientAddress": patientAddress,
                "patientAge": patientAge,
                "patientGender": patientGender,
                "patientName": patientName,
                "physicianId": physicianId,
                "physicianLicenseNumber": physicianLicenseNumber,
                "physicianName": physicianName,
                "prescriptionNumber": prescriptionNumber,
                "residentId": residentId
            }
        },
        headers: {
            "Accept":"application/json",
            "Content-Type": 'application/json',
        },
        json: true
    };
    
    requestPromise(options)
        .then(function (body) {
            console.log("Attributes Set");
    })
    .catch(function (err) {
        console.log(err.message);
    });

    for(var i = 0; i < items.length; i++) {
        item_list += ("<a data-toggle=modal href=#item-modal class=item-details data-id=" + i + ">" + items[i].medicationName + "</a>" + ((i + 1 < items.length)? "<br/>" : ""));
        price_list += ((parseFloat(items[i].price)*parseFloat(items[i].quantity)).toFixed(2).toString() + ((i + 1 < items.length)? "<br/>" : ""));
        qty_list += (items[i].quantity + ((i + 1 < items.length)? "<br/>" : ""));
        total_price += (parseFloat(items[i].price) + parseFloat(items[i].tax))*parseFloat(items[i].quantity) + parseFloat(items[i].shipping_cost);
        total_tax += parseFloat(items[i].tax);
        total_shipping_cost += parseFloat(items[i].shipping_cost);

        elements.push({  
            "title": items[i].medicationName,
            "subtitle": items[i].medicationDosage,
            "quantity": qty_list.split("<br/>")[i],
            "price": items[i].price.toFixed(2),
            "currency": currency,
            "image_url": "https://www.chemist-4-u.com/media/catalog/product/cache/1/image/450x/9df78eab33525d08d6e5fb8d27136e95/p/a/paracetamol_tablets_500mg/Paracetamol_Tablets_500mg_-_32_Tablets_(Brand_May_Vary)_31.jpg"
            });
        medData.push({
            "dosage": items[i].medicationDosage,
            "form": items[i].medicationForm,
            "ingredients": items[i].medicationIngredients,
            "name": items[i].medicationName,
            "packageSize": items[i].medicationPackageSize,
            "tax": currency + " " + items[i].tax.toFixed(2),
            "shipping_cost": currency + " " + items[i].shipping_cost.toFixed(2),
            "price": currency + " " + items[i].price.toFixed(2)
            });
        }
    res.render(path.join(__dirname, '../webview/index.html'),{
        userID:userID,
        is_paid:is_paid,
        payment_id:payment_id,
        patientName:patientName,
        item_list:item_list,
        qty_list:qty_list,
        price_list:price_list,
        currency:currency,
        adjustments:parseFloat(adjustments).toFixed(2).toString(),
        medData:JSON.stringify(medData),
        address:patientAddress,
        elements:JSON.stringify(elements),
        total_price:(total_price - parseFloat(adjustments)).toFixed(2).toString(),
        total_tax:total_tax.toFixed(2).toString(),
        total_shipping_cost:total_shipping_cost.toFixed(2).toString(),
        restUrl:routerDispergo.restUrl});

    /*
    model.fetch_order_details(
        function (patientName, item_list, qty_list, price_list, address, elements, total_price, medData, otherDetails) {
            if(arguments.length == 0) {
                console.log("ERROR RESPONSE");
                res.send("Error: Cannot load data. Please try again");
            } else {

            }
    }, type,
        order_id,
        currency);*/
});

subRouterDispergo.get('/ccard', function(req, res) {
    var userID = req.query.userID;
    var payment_id = "";//req.query.payment_id ;
    var trackingCode = req.query.trackingCode;
    var currency = req.query.currency;
    var items = JSON.parse(req.query.items);
    var patientAddress = req.query.patientAddress;
    var patientAge = req.query.patientAge;
    var patientGender = req.query.patientGender;
    var patientName = req.query.patientName;
    var physicianId = req.query.physicianId;
    var physicianLicenseNumber = req.query.physicianLicenseNumber;
    var physicianName = req.query.physicianName;
    var prescriptionNumber = req.query.prescriptionNumber;
    var residentId = req.query.residentId;
    var ecr_link = req.query.ecr_link;
    var is_paid = "false";
    var price_list = "";
    var item_list = "";
    var qty_list = "";
    var total_price = 0.0;
    var total_tax = 0.0;
    var total_shipping_cost = 0.0;
    var adjustments = req.query.adjustments;
    var elements = [];
    var medData = [];

    var options = {
        method: 'POST',
        uri: routerDispergo.restUrl + "/notify_order",
        body: {
            messenger_id : userID,
            blockName : "Blank",
            attributes : {
                "trackingCode" : trackingCode,
                "ecr_link": ecr_link,
                "items": items,
                "patientAddress": patientAddress,
                "patientAge": patientAge,
                "patientGender": patientGender,
                "patientName": patientName,
                "physicianId": physicianId,
                "physicianLicenseNumber": physicianLicenseNumber,
                "physicianName": physicianName,
                "prescriptionNumber": prescriptionNumber,
                "residentId": residentId
            }
        },
        headers: {
            "Accept":"application/json",
            "Content-Type": 'application/json',
        },
        json: true
    };
    
    requestPromise(options)
        .then(function (body) {
            console.log("Attributes Set");
    })
    .catch(function (err) {
        console.log(err.message);
    });

    for(var i = 0; i < items.length; i++) {
        item_list += ("<a data-toggle=modal href=#myModal class=item-details data-id=" + i + ">" + items[i].medicationName + "</a>" + ((i + 1 < items.length)? "<br/>" : ""));
        price_list += ((parseFloat(items[i].price)*parseFloat(items[i].quantity)).toFixed(2).toString() + ((i + 1 < items.length)? "<br/>" : ""));
        qty_list += (items[i].quantity + ((i + 1 < items.length)? "<br/>" : ""));
        total_price += (parseFloat(items[i].price) + parseFloat(items[i].tax))*parseFloat(items[i].quantity) + parseFloat(items[i].shipping_cost);
        total_tax += parseFloat(items[i].tax);
        total_shipping_cost += parseFloat(items[i].shipping_cost);

        elements.push({  
            "title": items[i].medicationName,
            "subtitle": items[i].medicationDosage,
            "quantity": qty_list.split("<br/>")[i],
            "price": items[i].price.toFixed(2),
            "currency": currency,
            "image_url": "https://www.chemist-4-u.com/media/catalog/product/cache/1/image/450x/9df78eab33525d08d6e5fb8d27136e95/p/a/paracetamol_tablets_500mg/Paracetamol_Tablets_500mg_-_32_Tablets_(Brand_May_Vary)_31.jpg"
            });
        medData.push({
            "dosage": items[i].medicationDosage,
            "form": items[i].medicationForm,
            "ingredients": items[i].medicationIngredients,
            "name": items[i].medicationName,
            "packageSize": items[i].medicationPackageSize,
            "tax": currency + " " + items[i].tax.toFixed(2),
            "shipping_cost": currency + " " + items[i].shipping_cost.toFixed(2),
            "price": currency + " " + items[i].price.toFixed(2)
            });
        }
    res.render(path.join(__dirname, '../webview/payment.html'),{
        userID:userID,
        is_paid:is_paid,
        payment_id:payment_id,
        patientName:patientName,
        item_list:item_list,
        qty_list:qty_list,
        price_list:price_list,
        currency:currency,
        adjustments:parseFloat(adjustments).toFixed(2).toString(),
        medData:JSON.stringify(medData),
        address:patientAddress,
        elements:JSON.stringify(elements),
        total_price:(total_price - parseFloat(adjustments)).toFixed(2).toString(),
        total_tax:total_tax.toFixed(2).toString(),
        total_shipping_cost:total_shipping_cost.toFixed(2).toString(),
        restUrl:routerDispergo.restUrl});

    /*
    model.fetch_order_details(
        function (patientName, item_list, qty_list, price_list, address, elements, total_price, medData, otherDetails) {
            if(arguments.length == 0) {
                console.log("ERROR RESPONSE");
                res.send("Error: Cannot load data. Please try again");
            } else {

            }
    }, type,
        order_id,
        currency);*/
});



subRouterDispergo.post('/confirmation', function(req, res) {
    let bulkDocs = {};
    let documents = [];
    let otherDetails = req.body;

    const url = 'http://172.104.176.10:4985/awhdispergodb/_bulk_docs';
    documents =  {
        "currentStatus":{
           "reason":"rc3",
           "status":"PENDING",
           "statusDate":"2019-10-04T03:21:38.510Z",
           "userDisplayName":"string string string",
           "username":"test@gmail.com"
        },
        otherDetails
    };
    bulkDocs = { "docs" : documents };
    /*
    var options = {
        method: 'POST',
        uri: url,
        body: bulkDocs,
        headers: {
            "Accept": 'application/json',
            "Content-Type": 'application/json'
        },
        json: true
    };

    requestPromise(options)
        .then(function (body) {
            console.log(body);
            res.send({"status" : body.status})
    })
    .catch(function (err) {
        console.log(err.message)
        res.send({"status" : "error"})
    });*/
   res.send({});
});

/***********PAYMENT SECTION END*********************************/
var routerDispergo = {
    subRouterDispergo,
    restUrl
}
module.exports = routerDispergo;