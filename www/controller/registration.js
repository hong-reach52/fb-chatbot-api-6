const express = require('express');
const routerRegistration = require('express').Router();
const model = require("../model/datahandler");
const randomstring = require("randomstring");

routerRegistration.post('/registration_details', function(req, res) {
    console.log("routerRegistration post");
    console.log(req.body);
    var jsonResponse = {};
    var languages = ["english", "hiligayon"];
    var langBlockRedirect = {
        "has_4p_yes": ["yes", "oo"],
        "has_4p_no": ["no", "wala"]
    };
    var proceedLangIdx = 0;
    var langPrefix = "Eng";

    if (req.body.lang !== 'undefined') {
        if (req.body.lang.toString().toLowerCase() == "english") {
            proceedLangIdx = 0;
            langPrefix = "Eng";
        }
        if (req.body.lang.toString().toLowerCase() == "hiligayon") {
            proceedLangIdx = 1;
            langPrefix = "Hil";
        }
    }

    if (typeof req.body.has_4p !== 'undefined') {
        if (req.body.has_4p.toString().toLowerCase() == langBlockRedirect.has_4p_yes[proceedLangIdx])
            jsonResponse = {
                "redirect_to_blocks": [langPrefix + " Registration Part 3-1"]
            }
        if (req.body.has_4p.toString().toLowerCase() == langBlockRedirect.has_4p_no[proceedLangIdx])
            jsonResponse = {
                "redirect_to_blocks": [langPrefix + " Registration Part 3-2"]
            }
    }
    console.log(jsonResponse);
    res.send(jsonResponse);
});

routerRegistration.post('/save_reg_details', function(req, res) {
    console.log("routerRegistration post");   
    var jsonResponse = {};
    
    var first_name = req.body.first_name;
    var middle_name = req.body.middle_name;
    var last_name = req.body.last_name;
    var gender = req.body.sex;
    var b_date = req.body.b_date;
    var cel_no = req.body.cel_no;
    var email = req.body.email;
    var address = req.body.address;
    var four_p_num = req.body.four_p_num;
    var gov_id_type = req.body.gov_id_type;
    var gov_id = req.body.gov_id;
    var messenger_id = req.body.messenger_id;
    var curis_id = randomstring.generate({
  		   	length: 8,
  		   	charset: 'numeric'
		   });
    
    var regDetails = {
                            messenger_id : messenger_id,
                            first_name : first_name ,
                            middle_name : middle_name ,
                            last_name : last_name,
                            gender: gender,
                            b_date : b_date,
                            cel_no : cel_no ,
                            email : email ,
                            address : address ,
                            four_p_num : four_p_num ,
                            gov_id_type : gov_id_type ,
                            gov_id : gov_id,
                            curis_id : curis_id 
                     };


    model.save_data("registration", regDetails);

    var jsonResponse = {
 			"messages": [
   					{"text": "Your Curis ID is: "+ curis_id + "\nYour registered data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});
module.exports = routerRegistration;