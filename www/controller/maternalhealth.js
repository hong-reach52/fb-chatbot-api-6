const express = require('express');
const routerMaternalHealth = require('express').Router();
const model = require("../model/datahandler");

var errorInputMessage = {
                          "Eng" : "You have input invalid choices. Please try again",
                          "Hil" : "Sala nga choices ang imo gin-enter. Palihog liwat sang imo sabat."
}

routerMaternalHealth.post('/maternal_health', function(req, res) {
    console.log("routerMaternalHealth post");
    var lang = req.body.lang.substring(0,3);
    var is_tet_vacc = req.body.is_tet_vacc;
    var preg_exp = req.body.preg_exp;
    var jsonResponse = {};
    
    if(typeof is_tet_vacc !== "undefined") {
        if(is_tet_vacc.split(",").some(isNaN) &&
           is_tet_vacc.toLowerCase() != "none" &&
           is_tet_vacc.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Maternal health 2"]
			};
        } else {
            jsonResponse = {
                                //"redirect_to_blocks": [lang + " Maternal health 3"]
			};
        }
    }
    
    if(typeof preg_exp !== "undefined") {
        if(preg_exp.split(",").some(isNaN) &&
           preg_exp.toLowerCase() != "none" &&
           preg_exp.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Maternal health 4"]
			};
        } else {
            jsonResponse = {
                                //"redirect_to_blocks": [lang + " Maternal health 4-0"]
			};
        }
    }

    console.log(jsonResponse);
    res.send(jsonResponse);
});

routerMaternalHealth.post('/save_maternal_health', function(req, res) {
    console.log("routerMaternalHealth post");   
    var jsonResponse = {};

    model.save_data("maternal_info", req.body);

    var jsonResponse = {
 			"messages": [
   					{"text": "Your maternal health info data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});


module.exports = routerMaternalHealth;