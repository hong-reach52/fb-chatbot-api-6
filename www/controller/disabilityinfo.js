const express = require('express');
const routerDisabilityInfo = require('express').Router();
const model = require("../model/datahandler");

var errorInputMessage = {
                          "Eng" : "You have input invalid choices. Please try again",
                          "Hil" : "Sala nga choices ang imo gin-enter. Palihog liwat sang imo sabat."
}

routerDisabilityInfo.post('/disability_info', function(req, res) {
    console.log("routerDisabilityInfo post");
    var lang = req.body.lang.substring(0,3);
    var disability = req.body.disability;
    var disease_history = req.body.disease_history;
    var jsonResponse = {};
    
    if(typeof disability !== "undefined") {
        if(disability.split(",").some(isNaN) &&
           disability.toLowerCase() != "none" &&
           disability.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Disability 2"]
			};
        } else {
            jsonResponse = {};
        }
    }

    console.log(jsonResponse);
    res.send(jsonResponse);
});

routerDisabilityInfo.post('/save_disability_info', function(req, res) {
    console.log("routerDisabilityInfo post");   
    var jsonResponse = {};

    model.save_data("disability_info", req.body);

    var jsonResponse = {
 			"messages": [
   					{"text": "Your disability info data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});


module.exports = routerDisabilityInfo;