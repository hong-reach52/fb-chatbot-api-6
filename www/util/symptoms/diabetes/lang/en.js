module.exports = async (survey) => {
    let questions = [
        "🟩⬜⬜⬜⬜⬜⬜\n"+"1. Do you feel very thirsty (even without physical activity)?\n\n1. Yes\n2. No",
        "🟩🟩⬜⬜⬜⬜⬜\n"+"2. Do you urinate more frequently than usual?\n\n1. Yes\n2. No",
        "🟩🟩🟩⬜⬜⬜⬜\n"'3. Have you dropped weight suddenly (without any strenuous activity or diet involved)?\n\n1. Yes\n2. No',
        "🟩🟩🟩🟩⬜⬜⬜\n"'4. Do you have cuts/wounds that heal slowly?\n\n1. Yes\n2. No',
        "🟩🟩🟩🟩🟩⬜⬜\n"+'5. Do you frequently experience sudden blurring of vision (not caused by bad sight)?\n\n1. Yes\n2. No',
        "🟩🟩🟩🟩🟩🟩⬜\n"+'6. Are you currently overweight or obese? (check your BMI)\n\n1. Yes\n2. No',
        "🟩🟩🟩🟩🟩🟩🟩\n"+'7. Do you have a close relative diagnosed with Diabetes?\n\n1. Yes\n2. No'
    ]

    let symCount = await require("../analyse")(survey, questions)

    let response = ""
    switch (symCount) {
        case 0:
            response = "Based on your answers, you are not presenting any symptoms relating to Diabetes nor presenting any risk factor for it. These are just some common symptoms/risk factors and at times Diabetes is even asymptomatic, you can consult your doctor to get a full diagnosis. You can also book a rapid screening to check your blood glucose through this service by pressing 2. Or reply 0 to return to main menu."
            break;
        case 1:
            response = "Based on your answers, you are not presenting any symptoms relating to Diabetes but you do present a risk factor/s and this should be a concern as it increases your chances of developing the disease. I highly advise you consult with a doctor to get on top of your health. You can also book a rapid screening to check your blood glucose through this service by pressing 2. Or reply 0 to return to main menu."
            break;
        case 2:
            response = "Based on your answers, you are presenting some symptoms relating to Diabetes. Although a few, this should still be a concern, consult a doctor to get a full diagnosis. You can also book a rapid screening to test your blood glucose through this service by pressing 2. Or reply 0 to return to main menu"
            break;
        case 3:
            response = "Based on your answers, you are presenting some symptoms relating to Diabetes. Although a few, you are also presenting a risk factor/s which increases the possiblity of developing the disease. Get on top of it by consulting a doctor to get a full diagnosis. You can also book a rapid screening to test your blood glucose through this service by pressing 2. Or reply 0 to return to main menu"
            break;
        case 4:
            response = "Based on your answers, you are presenting symptoms relating to Diabetes. I highly advise you to get a consultation from your doctor for a full diagnosis or book a rapid screening to test your blood glucose through this service by pressing 2. Or reply 0 to return to main menu"
            break;
        case 5:
        case 6:
        case 7:
            response = "Based on your answers, you are presenting symptoms relating to Diabetes and risk factor/s that increase the possibility of developing the disease. I highly advise you to get a consultation from your doctor for a full diagnosis or book a rapid screening to test your blood glucose through this service by pressing 2. Or reply 0 to return to main menu"
            break;
        default:
            response = "Sorry, you have submitted invalid responses along the way. Please type DIABCHECK to restart this symptom checker or 0 to return to the main menu."
            break;
    }

    return response
}